package com.atguigu.springcloud.service.impl;

import com.atguigu.springcloud.dao.PaymentDao;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    public int create(Payment payment) {
        int id= paymentDao.create(payment);
        if(payment.getSerial().equals("第8004调")){
            int i=1/0;
        }
        if(payment.getSerial().equals("第8004调延时")){
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String url8005="http://127.0.0.1:8005/payment/create";
        payment.setId(null);
        restTemplate.postForObject(url8005,payment,Object.class);
        return id;
    }

    @Override
    public Payment getPaymentById(Integer id) {
        return paymentDao.getPaymentById(id);
    }
}
