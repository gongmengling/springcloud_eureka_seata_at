package com.atguigu.springcloud.service.impl;

import com.atguigu.springcloud.dao.PaymentDao;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;
    @Autowired
    private RestTemplate restTemplate;

    @Override
    @GlobalTransactional(name = "seata-test-server", rollbackFor = Exception.class)
    public int create(Payment payment) {
        int id=paymentDao.create(payment);
        if(payment.getSerial().equals("第8001调")){
            int i=1/0;
        }
        if(payment.getSerial().equals("第8001调延时")){
            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String url8002="http://127.0.0.1:8002/payment/create";
        String url8004="http://127.0.0.1:8004/payment/create";
        restTemplate.postForObject(url8002,payment,Object.class);
        restTemplate.postForObject(url8004,payment,Object.class);
        return id;
    }

    @Override
    public Payment getPaymentById(Integer id) {
        return paymentDao.getPaymentById(id);
    }
}
